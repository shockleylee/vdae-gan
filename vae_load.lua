require 'hdf5'

function load(continuous)
    if continuous then
        return loadfreyfaces()
    else
        return loadmnist()
    end
    --return loadymovie()
end

function loadmnist()
    -- This loads an hdf5 version of the MNIST dataset used here: http://deeplearning.net/tutorial/gettingstarted.html
    -- Direct link: http://deeplearning.net/data/mnist/mnist.pkl.gz

    local f = hdf5.open('datasets/mnist.hdf5', 'r')

    data = {}
    data.train = f:read('x_train'):all():double()
    data.test = f:read('x_test'):all():double()
    data.y_train = f:read('t_train'):all():int()
    data.y_test = f:read('t_test'):all():int()
    local mask_y_train = torch.eq(data.y_train,2) + torch.eq(data.y_train,5)
    local mask_y_test = torch.eq(data.y_test,2) + torch.eq(data.y_test,5)
    local mask_x_train=mask_y_train:view(mask_y_train:size()[1],1):expandAs(data.train)
    local mask_x_test=mask_y_test:view(mask_y_test:size()[1],1):expandAs(data.test)


    data.train = data.train[mask_x_train]:view(-1,data.train:size()[2])
    data.y_train = data.y_train[mask_y_train]
    data.y_train[torch.eq(data.y_train,2)] = 1
    data.y_train[torch.eq(data.y_train,5)] = 2

    data.test = data.test[mask_x_test]:view(-1,data.test:size()[2])
    data.y_test = data.y_test[mask_y_test]
    data.y_test[torch.eq(data.y_test,2)] = 1
    data.y_test[torch.eq(data.y_test,5)] = 2
    f:close()

    return data
end

function loadfreyfaces()
    require 'hdf5'
    local f = hdf5.open('datasets/freyfaces.hdf5', 'r')
    local data = {}
    data.train = f:read('train'):all():double()
    data.test = f:read('test'):all():double()
    f:close()

    return data
end

function loadymovie()
    -- This loads an hdf5 version of the MNIST dataset used here: http://deeplearning.net/tutorial/gettingstarted.html
    -- Direct link: http://deeplearning.net/data/mnist/mnist.pkl.gz

    local f = hdf5.open('datasets/ymovie_d.hdf5', 'r')

    data = {}
    data.train = f:read('x_train'):all():double()
    data.test = f:read('x_test'):all():double()
    data.y_train = f:read('y_train'):all():double()
    data.y_test = f:read('y_test'):all():double()

    f:close()

    return data
end
