import numpy as np
from scipy.sparse import *
import random
import warnings,sklearn.datasets as datasets
from sklearn.cross_validation import train_test_split
import h5py

def readfile(f):
  with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    (x, y) = datasets.load_svmlight_file(f, zero_based=False)
  print "\"data size", x.shape, "sparsity", float(x.nnz)/(x.shape[0]*x.shape[1]), "\""
  return (x,y)


if __name__ == '__main__':
  datafile = 'datasets/ymovie_d.sparse'
  (x,y) = readfile(datafile)
  print x.max()
  x = x/x.max()

  y[y==1] = 1
  y[y==-1] = 2
  x_train, x_test, y_train, y_test = train_test_split(x, y, test_size = 0.2, random_state = int(100 * random.random()))
  f = h5py.File('datasets/ymovie_d.hdf5', 'w')
  f.create_dataset('x_train', data=x_train.todense())
  f.create_dataset('x_test', data=x_test.todense())
  f.create_dataset('y_train', data=y_train)
  f.create_dataset('y_test', data=y_test)
  f.close()
